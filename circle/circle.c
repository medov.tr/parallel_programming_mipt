#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define r "[rank=%d] "

int main(int argc, char* argv[])
{
    int commsize, my_rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    char buff[28]={};
    int i;
    for(i = 0; i < 28; ++i)
        buff[i] = i; // define our array

    if(my_rank == 0) // my_rank == 0 is the main process
    {
        if(commsize > 1) // checking for the number of processes
        {

            int res = MPI_Send(buff, sizeof(buff), MPI_CHAR, my_rank+1, // send a message to the following process
                               1, MPI_COMM_WORLD);

            if(res != MPI_SUCCESS) // checking for success
                fprintf(stderr, r" Error ocurred while sending from %d to %d\n",
                                my_rank, my_rank, my_rank + 1);
        }
        else // checking for the number = 1
        {
            fprintf(stderr, " Too few compute elements\n"); 
            MPI_Finalize();
            exit(0);
        }



        int res = MPI_Recv(buff, sizeof(buff), MPI_CHAR, MPI_ANY_SOURCE,
                           MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        if(res != MPI_SUCCESS) // checking for success
            fprintf(stderr, r" Error ocurred while receiving\n",
                            my_rank);
        else
            fprintf(stdout, r" Received number \"%hhu\"\n",
                            my_rank, buff[my_rank]);
    }
    else
    {
        int dest = 0;

        if(my_rank < commsize - 1) // checking for the last process
            dest = my_rank + 1;

        int res = MPI_Recv(buff, sizeof(buff), MPI_CHAR, MPI_ANY_SOURCE,
                           MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        if(res != MPI_SUCCESS) // checking for success
            fprintf(stderr, r" Error ocurred while receiving\n",
                            my_rank);
        else
            fprintf(stdout, r" Received number \"%hhu\"\n",
                            my_rank, buff[my_rank]);


        res = MPI_Send(buff, sizeof(buff), MPI_CHAR, dest, // send a message to the following process (dest = my_rank + 1)
                       1, MPI_COMM_WORLD);

        if(res != MPI_SUCCESS) // checking for success
            fprintf(stderr, r" Error ocurred while sending from %d to %d\n",
                            my_rank, my_rank, dest);
    }


    MPI_Finalize();
    return 0;
}