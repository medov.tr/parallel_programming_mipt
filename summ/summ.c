#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define r "[rank=%d] "

int main(int argc, char* argv[])
{
    int commsize, my_rank;                                                      
    MPI_Init(&argc, &argv);                                                     
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    int i;

    if(my_rank == 0) // my_rank == 0 is the main process
    {
        if(argc < 2)
        {
            printf(r"\"N\" was not provided\n", my_rank);
            exit(EXIT_FAILURE);
        }

        int N = atoi(argv[1]); // accept a natural number from the command line

        if(N == 0) // checking for valid input
        {
            printf(r"\"N\" is not a number or equal 0\n", my_rank);
            exit(EXIT_FAILURE);
        }

        long double sum = 0;

        int range = N / (commsize - 1); // determine the number of intervals,
                                       // (commsize - 1), because the zero rank will not count

        for(i = 1; i <= commsize-1; i ++) // in the loop, we send an interval to each process except the zero one
        {
            int start = (i - 1) * range; // determine the beginning of the interval
            int finish = i * range;      // determine the end of the interval

            int res = MPI_Send(&start, 1, MPI_INT, i, 0, MPI_COMM_WORLD); // send the i process its start

            if(res != MPI_SUCCESS) // checking for success
                fprintf(stderr, r"Error ocurred while sending start point to %d\n",
                                            my_rank, i);

            if(i == (commsize - 1)) // determine the end of the interval for the last process
                finish = N;

            res = MPI_Send(&finish, 1, MPI_INT, i, 0, MPI_COMM_WORLD); // // send the i process its finish

            if(res != MPI_SUCCESS) // checking for success 
                fprintf(stderr, r"Error ocurred while sending finish point to %d\n",
                                            my_rank, i);
        }

        for(i = 1; i <= commsize-1; i ++) // the loop that receives the messages
        { 
            long double temp = 0;

            int res = MPI_Recv(&temp, 1, MPI_LONG_DOUBLE, i, MPI_ANY_TAG,
                               MPI_COMM_WORLD, MPI_STATUS_IGNORE);


            if(res != MPI_SUCCESS)
                fprintf(stderr, r"Error ocurred while getting number from %d\n",
                                            my_rank, i);
            else
                fprintf(stdout, r"Got number \"%Lf\" from rank %d\n",
                                my_rank, temp, i);

            sum += temp; // summarize the result of each process

        }

        fprintf(stdout, r"Result is \"%Lf\"\n", my_rank, sum);
    }

    else // all other processes
    {
        if(argc < 2)
            exit(EXIT_FAILURE);

        int N = 0;

        if((N = atoi(argv[1])) == 0)
            exit(EXIT_FAILURE);

        long double sum = 0;

        int start = -1;
        int finish = -1;

        int res = MPI_Recv(&start, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE); // get the beginning of the interval

        if(res != MPI_SUCCESS) // checking for success 
            fprintf(stderr, r"Error ocurred while getting start point from %d\n",
                                            my_rank, 0);

        res = MPI_Recv(&finish, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE); // get the end of the interval

        if(res != MPI_SUCCESS) // checking for success 
            fprintf(stderr, r"Error ocurred while getting finish point from %d\n",
                                            my_rank, 0);

        fprintf(stdout, r"My range is (%d, %d]\n",
                        my_rank, start, finish);

        
        for(i = finish; i > start; i--) // sum up all the values of this interval
            sum += i;

        res = MPI_Send(&sum, 1, MPI_LONG_DOUBLE, 0, 0, MPI_COMM_WORLD); // sending the result

        if(res != MPI_SUCCESS) // checking for success 
            fprintf(stderr, r"Error ocurred while sending from %d to %d\n",
                                        my_rank, my_rank, 0);
        else
            fprintf(stdout, r"Sent \"%Lf\" to rank %d\n",
                            my_rank, sum, 0);

    }

    MPI_Finalize();

    return 0;
}